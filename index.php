<?php
// Connexion à la base de données...
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "todolist";

$connection = new mysqli($servername, $username, $password, $dbname);

// Vérifier la connexion
if ($connection->connect_error) {
    die("La connexion a échoué : " . $connection->connect_error);
}

// Ajout d'une nouvelle tâche
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['title'])) {
    $title = $_POST['title'];
    $insertQuery = "INSERT INTO todo (title) VALUES ('$title')";
    if ($connection->query($insertQuery) === TRUE) {
        // Redirection après l'ajout d'une tâche pour éviter les re-soumissions de formulaire
        header("Location: ".$_SERVER['PHP_SELF']);
        exit();
    } else {
        echo "Erreur lors de l'ajout de la tâche : " . $connection->error;
    }
}

// Modification de l'état 'done' d'une tâche
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['action']) && $_POST['action'] == 'toggle') {
    $taskId = $_POST['id'];
    $toggleQuery = "UPDATE todo SET done = 1 - done WHERE id = $taskId";
    if ($connection->query($toggleQuery) === TRUE) {
        // Redirection après la modification de l'état pour mettre à jour l'affichage
        header("Location: ".$_SERVER['PHP_SELF']);
        exit();
    } else {
        echo "Erreur lors de la modification de l'état de la tâche : " . $connection->error;
    }
}

// Suppression d'une tâche
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['action']) && $_POST['action'] == 'delete') {
    $taskId = $_POST['id'];
    $deleteQuery = "DELETE FROM todo WHERE id = $taskId";
    if ($connection->query($deleteQuery) === TRUE) {
        // Redirection après la suppression pour mettre à jour l'affichage
        header("Location: ".$_SERVER['PHP_SELF']);
        exit();
    } else {
        echo "Erreur lors de la suppression de la tâche : " . $connection->error;
    }
}

// Récupération des tâches
$query = "SELECT * FROM todo ORDER BY created_at DESC";
$result = $connection->query($query);

$taches = [];
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $taches[] = $row;
    }
}

$connection->close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- En-tête, liens CSS Bootstrap, etc. -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Ma Liste de Tâches</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <!-- Contenu du Navbar -->
    </nav>
    <!-- Formulaire d'ajout de tâches -->
    <form action="" method="post">
        <div class="input-group mt-3">

            <input type="text" name="title" placeholder="Nouvelle tâche" class="form-control">
            <button type="submit" class="btn btn-primary">Ajouter</button>
        </div>
    </form>

    <!-- Liste des tâches -->
    <ul class="list-group mt-3">
        <?php foreach ($taches as $tache) : ?>
            <li style="color: <?php echo $tache['done'] ? 'green' : 'black'; ?>">
                <?php echo $tache['title']; ?>

                <!-- Formulaires pour chaque tâche -->
                <form action="" method="post" class="float-end">
                    <input type="hidden" name="id" value="<?php echo $tache['id']; ?>">
                    <button type="submit" name="action" value="toggle" class="btn btn-sm btn-success">Done</button>
                    <button type="submit" name="action" value="delete" class="btn btn-sm btn-danger">Supprimer</button>
                </form>
            </li>
        <?php endforeach; ?>
    </ul>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Scripts JavaScript, autres balises HTML, etc. -->
</body>
</html>
